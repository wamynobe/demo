import 'package:get/get.dart';

import '../../../data/photo/models/photo_model.dart';
import '../../../data/repository/repository.dart';
import '../../../data/service/service_locator.dart';

class HomeController extends GetxController {
  final photoRepos = getIt<IRepository<PhotoModel>>();
  final count = 0.obs;
  final message = ''.obs;
  final photo = Rxn<PhotoModel>();
  void increment() => count.value++;
  Future<void> fetchPhoto() async {
    (await photoRepos.getOne(
      '/photos',
    ))
        .fold(
      (l) => message.value = l.when(
        get: () => 'cannot get data',
        post: () => 'cannot post data',
        delete: () => 'cannot delete data',
        put: () => 'cannot put data',
        convert: () => 'cannot convert data',
        server: () => 'cannot connect to server',
        internet: () => 'internet fail',
        unknow: () => 'unknow exception',
      ),
      (r) => photo.value = r,
    );
  }

  @override
  Future<void> onInit() async {
    super.onInit();
    await fetchPhoto();
    update();
  }
}
