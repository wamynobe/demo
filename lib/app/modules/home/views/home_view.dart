import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  HomeView({super.key});
  final homeController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('testPageTileLabel'.tr),
        centerTitle: true,
      ),
      body: GetBuilder(
        init: homeController,
        builder: (controller) => controller.photo.value != null
            ? ListTile(
                title: SizedBox(
                  width: 100,
                  height: 100,
                  child: CachedNetworkImage(
                    imageUrl: controller.photo.value?.url ?? '',
                  ),
                ),
                trailing: Text(
                  controller.photo.value?.title ?? '',
                ),
              )
            : Center(
                child: Text(controller.message.value),
              ),
      ),
    );
  }
}
