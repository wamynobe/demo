import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'value/value.dart';

class Localization extends Translations {
  //get locale when app run
  static final locale = _getLocaleFromLanguage();

  //default locale
  static const kFallBackLocale = Locale('de', 'DE');

  static final langCodes = ['en', 'vi', 'de', 'cs'];

  //get supported locale
  static const locales = [
    Locale('en', 'UK'),
    Locale('vi', 'VN'),
    Locale('de', 'DE'),
    Locale('cs', 'CZ'),
  ];

  static void changeLocale(String langCode) {
    final locale = _getLocaleFromLanguage(langCode: langCode);
    Get.updateLocale(locale);
  }

  @override
  Map<String, Map<String, String>> get keys => {
        'en_UK': en,
        'vi_VN': vi,
        'cs_CZ': cs,
        'de_DE': de,
      };
  static Locale _getLocaleFromLanguage({String? langCode}) {
    final lang = langCode ?? Get.deviceLocale?.languageCode;
    final listLocales =
        locales.map((e) => lang == e.languageCode ? e : kFallBackLocale);
    return listLocales.isNotEmpty ? listLocales.toList()[0] : kFallBackLocale;
  }
}
