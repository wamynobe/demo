import 'package:freezed_annotation/freezed_annotation.dart';

part 'repository_failure.freezed.dart';

@freezed
class RepositoryFailure with _$RepositoryFailure {
  const factory RepositoryFailure.get() = _get;
  const factory RepositoryFailure.post() = _post;
  const factory RepositoryFailure.delete() = _delete;
  const factory RepositoryFailure.put() = _put;
  const factory RepositoryFailure.convert() = _convert;
  const factory RepositoryFailure.server() = _server;
  const factory RepositoryFailure.internet() = _internet;
  const factory RepositoryFailure.unknow() = _unknow;
}
