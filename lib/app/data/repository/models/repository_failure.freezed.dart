// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'repository_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RepositoryFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepositoryFailureCopyWith<$Res> {
  factory $RepositoryFailureCopyWith(
          RepositoryFailure value, $Res Function(RepositoryFailure) then) =
      _$RepositoryFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$RepositoryFailureCopyWithImpl<$Res>
    implements $RepositoryFailureCopyWith<$Res> {
  _$RepositoryFailureCopyWithImpl(this._value, this._then);

  final RepositoryFailure _value;
  // ignore: unused_field
  final $Res Function(RepositoryFailure) _then;
}

/// @nodoc
abstract class _$$_getCopyWith<$Res> {
  factory _$$_getCopyWith(_$_get value, $Res Function(_$_get) then) =
      __$$_getCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_getCopyWithImpl<$Res> extends _$RepositoryFailureCopyWithImpl<$Res>
    implements _$$_getCopyWith<$Res> {
  __$$_getCopyWithImpl(_$_get _value, $Res Function(_$_get) _then)
      : super(_value, (v) => _then(v as _$_get));

  @override
  _$_get get _value => super._value as _$_get;
}

/// @nodoc

class _$_get implements _get {
  const _$_get();

  @override
  String toString() {
    return 'RepositoryFailure.get()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_get);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) {
    return get();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) {
    return get?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) {
    if (get != null) {
      return get();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) {
    return get(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) {
    return get?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) {
    if (get != null) {
      return get(this);
    }
    return orElse();
  }
}

abstract class _get implements RepositoryFailure {
  const factory _get() = _$_get;
}

/// @nodoc
abstract class _$$_postCopyWith<$Res> {
  factory _$$_postCopyWith(_$_post value, $Res Function(_$_post) then) =
      __$$_postCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_postCopyWithImpl<$Res> extends _$RepositoryFailureCopyWithImpl<$Res>
    implements _$$_postCopyWith<$Res> {
  __$$_postCopyWithImpl(_$_post _value, $Res Function(_$_post) _then)
      : super(_value, (v) => _then(v as _$_post));

  @override
  _$_post get _value => super._value as _$_post;
}

/// @nodoc

class _$_post implements _post {
  const _$_post();

  @override
  String toString() {
    return 'RepositoryFailure.post()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_post);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) {
    return post();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) {
    return post?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) {
    if (post != null) {
      return post();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) {
    return post(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) {
    return post?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) {
    if (post != null) {
      return post(this);
    }
    return orElse();
  }
}

abstract class _post implements RepositoryFailure {
  const factory _post() = _$_post;
}

/// @nodoc
abstract class _$$_deleteCopyWith<$Res> {
  factory _$$_deleteCopyWith(_$_delete value, $Res Function(_$_delete) then) =
      __$$_deleteCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_deleteCopyWithImpl<$Res>
    extends _$RepositoryFailureCopyWithImpl<$Res>
    implements _$$_deleteCopyWith<$Res> {
  __$$_deleteCopyWithImpl(_$_delete _value, $Res Function(_$_delete) _then)
      : super(_value, (v) => _then(v as _$_delete));

  @override
  _$_delete get _value => super._value as _$_delete;
}

/// @nodoc

class _$_delete implements _delete {
  const _$_delete();

  @override
  String toString() {
    return 'RepositoryFailure.delete()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_delete);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) {
    return delete();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) {
    return delete?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) {
    if (delete != null) {
      return delete();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) {
    return delete(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) {
    return delete?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) {
    if (delete != null) {
      return delete(this);
    }
    return orElse();
  }
}

abstract class _delete implements RepositoryFailure {
  const factory _delete() = _$_delete;
}

/// @nodoc
abstract class _$$_putCopyWith<$Res> {
  factory _$$_putCopyWith(_$_put value, $Res Function(_$_put) then) =
      __$$_putCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_putCopyWithImpl<$Res> extends _$RepositoryFailureCopyWithImpl<$Res>
    implements _$$_putCopyWith<$Res> {
  __$$_putCopyWithImpl(_$_put _value, $Res Function(_$_put) _then)
      : super(_value, (v) => _then(v as _$_put));

  @override
  _$_put get _value => super._value as _$_put;
}

/// @nodoc

class _$_put implements _put {
  const _$_put();

  @override
  String toString() {
    return 'RepositoryFailure.put()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_put);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) {
    return put();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) {
    return put?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) {
    if (put != null) {
      return put();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) {
    return put(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) {
    return put?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) {
    if (put != null) {
      return put(this);
    }
    return orElse();
  }
}

abstract class _put implements RepositoryFailure {
  const factory _put() = _$_put;
}

/// @nodoc
abstract class _$$_convertCopyWith<$Res> {
  factory _$$_convertCopyWith(
          _$_convert value, $Res Function(_$_convert) then) =
      __$$_convertCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_convertCopyWithImpl<$Res>
    extends _$RepositoryFailureCopyWithImpl<$Res>
    implements _$$_convertCopyWith<$Res> {
  __$$_convertCopyWithImpl(_$_convert _value, $Res Function(_$_convert) _then)
      : super(_value, (v) => _then(v as _$_convert));

  @override
  _$_convert get _value => super._value as _$_convert;
}

/// @nodoc

class _$_convert implements _convert {
  const _$_convert();

  @override
  String toString() {
    return 'RepositoryFailure.convert()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_convert);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) {
    return convert();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) {
    return convert?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) {
    if (convert != null) {
      return convert();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) {
    return convert(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) {
    return convert?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) {
    if (convert != null) {
      return convert(this);
    }
    return orElse();
  }
}

abstract class _convert implements RepositoryFailure {
  const factory _convert() = _$_convert;
}

/// @nodoc
abstract class _$$_serverCopyWith<$Res> {
  factory _$$_serverCopyWith(_$_server value, $Res Function(_$_server) then) =
      __$$_serverCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_serverCopyWithImpl<$Res>
    extends _$RepositoryFailureCopyWithImpl<$Res>
    implements _$$_serverCopyWith<$Res> {
  __$$_serverCopyWithImpl(_$_server _value, $Res Function(_$_server) _then)
      : super(_value, (v) => _then(v as _$_server));

  @override
  _$_server get _value => super._value as _$_server;
}

/// @nodoc

class _$_server implements _server {
  const _$_server();

  @override
  String toString() {
    return 'RepositoryFailure.server()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_server);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) {
    return server();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) {
    return server?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) {
    if (server != null) {
      return server();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) {
    return server(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) {
    return server?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) {
    if (server != null) {
      return server(this);
    }
    return orElse();
  }
}

abstract class _server implements RepositoryFailure {
  const factory _server() = _$_server;
}

/// @nodoc
abstract class _$$_internetCopyWith<$Res> {
  factory _$$_internetCopyWith(
          _$_internet value, $Res Function(_$_internet) then) =
      __$$_internetCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_internetCopyWithImpl<$Res>
    extends _$RepositoryFailureCopyWithImpl<$Res>
    implements _$$_internetCopyWith<$Res> {
  __$$_internetCopyWithImpl(
      _$_internet _value, $Res Function(_$_internet) _then)
      : super(_value, (v) => _then(v as _$_internet));

  @override
  _$_internet get _value => super._value as _$_internet;
}

/// @nodoc

class _$_internet implements _internet {
  const _$_internet();

  @override
  String toString() {
    return 'RepositoryFailure.internet()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_internet);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) {
    return internet();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) {
    return internet?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) {
    if (internet != null) {
      return internet();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) {
    return internet(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) {
    return internet?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) {
    if (internet != null) {
      return internet(this);
    }
    return orElse();
  }
}

abstract class _internet implements RepositoryFailure {
  const factory _internet() = _$_internet;
}

/// @nodoc
abstract class _$$_unknowCopyWith<$Res> {
  factory _$$_unknowCopyWith(_$_unknow value, $Res Function(_$_unknow) then) =
      __$$_unknowCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_unknowCopyWithImpl<$Res>
    extends _$RepositoryFailureCopyWithImpl<$Res>
    implements _$$_unknowCopyWith<$Res> {
  __$$_unknowCopyWithImpl(_$_unknow _value, $Res Function(_$_unknow) _then)
      : super(_value, (v) => _then(v as _$_unknow));

  @override
  _$_unknow get _value => super._value as _$_unknow;
}

/// @nodoc

class _$_unknow implements _unknow {
  const _$_unknow();

  @override
  String toString() {
    return 'RepositoryFailure.unknow()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_unknow);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() get,
    required TResult Function() post,
    required TResult Function() delete,
    required TResult Function() put,
    required TResult Function() convert,
    required TResult Function() server,
    required TResult Function() internet,
    required TResult Function() unknow,
  }) {
    return unknow();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
  }) {
    return unknow?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? get,
    TResult Function()? post,
    TResult Function()? delete,
    TResult Function()? put,
    TResult Function()? convert,
    TResult Function()? server,
    TResult Function()? internet,
    TResult Function()? unknow,
    required TResult orElse(),
  }) {
    if (unknow != null) {
      return unknow();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_get value) get,
    required TResult Function(_post value) post,
    required TResult Function(_delete value) delete,
    required TResult Function(_put value) put,
    required TResult Function(_convert value) convert,
    required TResult Function(_server value) server,
    required TResult Function(_internet value) internet,
    required TResult Function(_unknow value) unknow,
  }) {
    return unknow(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
  }) {
    return unknow?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_get value)? get,
    TResult Function(_post value)? post,
    TResult Function(_delete value)? delete,
    TResult Function(_put value)? put,
    TResult Function(_convert value)? convert,
    TResult Function(_server value)? server,
    TResult Function(_internet value)? internet,
    TResult Function(_unknow value)? unknow,
    required TResult orElse(),
  }) {
    if (unknow != null) {
      return unknow(this);
    }
    return orElse();
  }
}

abstract class _unknow implements RepositoryFailure {
  const factory _unknow() = _$_unknow;
}
