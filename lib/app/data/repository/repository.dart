import 'dart:convert';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import 'models/repository_failure.dart';
import 'models/serializable.dart';

abstract class IRepository<T extends Serializable<T>> {
  ///Get a model type [T] with [endPoint] is endpoint of url api.
  ///You can set [queryParam].
  Future<Either<RepositoryFailure, T>> getOne(
    String endPoint, {
    Map<String, dynamic>? queryParam,
  });

  ///Get a list model type [T] with [endPoint] is endpoint of url api.
  ///You can set [queryParam].
  Future<Either<RepositoryFailure, IList<T>>> getList(
    String endPoint, {
    Map<String, dynamic>? queryParam,
  });
  //TODO(huy): implement later
  Future<Either<RepositoryFailure, Unit>> create();
  //TODO(huy): implement later
  Future<Either<RepositoryFailure, Unit>> update();
  //TODO(huy): implement later
  ///Delete an resource by resource [id].
  Future<Either<RepositoryFailure, Unit>> delete();

  ///Get list resources.
  ///[field] is the field's name of the resource.
  ///[isEqualTo] equal to value of the resource's field value.
  ///[isNotEqualTo] not equal to value of the resource's field value.
  ///[isLessThan] less than value of the resource's field value.
  ///[isLessThanOrEqualTo] is less than or equal to field value.
  ///[isGreaterThan] is greater than field value.
  ///[isGreaterThanOrEqualTo] is greater or equal to field value.
  ///[isNull] the field with null value.
  Future<Either<RepositoryFailure, IList<T>>> where(
    String field,
    Object? isEqualTo,
    Object? isNotEqualTo,
    Object? isLessThan,
    Object? isLessThanOrEqualTo,
    Object? isGreaterThan,
    Object? isGreaterThanOrEqualTo,
    Object? isNull,
  );
  //TODO(huy.nq2) implement method update field later
}

abstract class Repository<T extends Serializable<T>> implements IRepository<T> {
  Repository({required Dio dio}) : _dio = dio;

  final Dio _dio;

  Either<RepositoryFailure, T> fromDocument(
    Map<String, dynamic> r,
    Function1<Map<String, dynamic>, T> factory,
  ) =>
      catching<T>(() => factory(r)).fold(
        (dynamic _) => left(const RepositoryFailure.convert()),
        right,
      );

  Function1<Map<String, dynamic>, T> dtoFactory();

  @override
  Future<Either<RepositoryFailure, T>> getOne(
    String endPoint, {
    Map<String, dynamic>? queryParam,
  }) async {
    try {
      final response =
          await _dio.get<String>(endPoint, queryParameters: queryParam);
      if (response.statusCode == 200) {
        final dataT = Map<String, dynamic>.from(
          json.decode(response.data ?? '{}') as Map<String, dynamic>,
        );
        final modelT = fromDocument(
          dataT,
          dtoFactory(),
        );
        return modelT;
      } else {
        return left(const RepositoryFailure.get());
      }
    } on DioError catch (_) {
      return left(const RepositoryFailure.server());
    } on SocketException catch (_) {
      return left(const RepositoryFailure.internet());
    } catch (_) {
      return left(const RepositoryFailure.unknow());
    }
  }

  @override
  Future<Either<RepositoryFailure, Unit>> create() {
    // TODO: implement create
    throw UnimplementedError();
  }

  @override
  Future<Either<RepositoryFailure, Unit>> update() {
    // TODO: implement update
    throw UnimplementedError();
  }

  @override
  Future<Either<RepositoryFailure, Unit>> delete() {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<Either<RepositoryFailure, IList<T>>> getList(String endPoint,
      {Map<String, dynamic>? queryParam}) {
    // TODO: implement getList
    throw UnimplementedError();
  }

  @override
  Future<Either<RepositoryFailure, IList<T>>> where(
      String field,
      Object? isEqualTo,
      Object? isNotEqualTo,
      Object? isLessThan,
      Object? isLessThanOrEqualTo,
      Object? isGreaterThan,
      Object? isGreaterThanOrEqualTo,
      Object? isNull) {
    // TODO: implement where
    throw UnimplementedError();
  }
}
