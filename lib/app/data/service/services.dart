import 'package:dio/dio.dart';

import '../../configs/api_configs.dart';

class DioService {
  static Dio dioCore() => Dio(
        BaseOptions(
          //todo implement url base
          baseUrl: baseUrl,
          sendTimeout: 60000,
          headers: <String, dynamic>{
            'Content-Type': 'application/json; charset=utf-8',
            //todo implement token here
            'Authorization': 'Bearer $appToken',
          },
        ),
      )..interceptors.add(
          InterceptorsWrapper(
            onRequest: (options, handler) => handler.next(options),
            onError: (e, handler) => handler.next(e),
          ),
        );
}
