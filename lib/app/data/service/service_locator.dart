import 'package:get_it/get_it.dart';

import '../photo/models/photo_model.dart';
import '../photo/repository/photo_repository.dart';
import '../repository/repository.dart';
import 'services.dart';

final getIt = GetIt.instance;

Future<void> setupLocator() async {
  getIt.registerLazySingleton<IRepository<PhotoModel>>(
    () => PhotoRepository(dio: DioService.dioCore()),
  );
}
