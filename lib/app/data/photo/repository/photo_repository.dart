import 'package:dartz/dartz.dart';

import '../../repository/models/repository_failure.dart';
import '../../repository/repository.dart';
import '../models/photo_model.dart';

class PhotoRepository extends Repository<PhotoModel> {
  PhotoRepository({required super.dio});
  @override
  Future<Either<RepositoryFailure, Unit>> create() {
    // TODO: implement create
    throw UnimplementedError();
  }

  @override
  Future<Either<RepositoryFailure, Unit>> delete() {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Function1<Map<String, dynamic>, PhotoModel> dtoFactory() =>
      PhotoModel.fromJson;

  @override
  Future<Either<RepositoryFailure, IList<PhotoModel>>> getList(
    String endPoint, {
    Map<String, dynamic>? queryParam,
  }) {
    // TODO: implement getList
    throw UnimplementedError();
  }

  @override
  Future<Either<RepositoryFailure, Unit>> update() {
    // TODO: implement update
    throw UnimplementedError();
  }

  @override
  Future<Either<RepositoryFailure, IList<PhotoModel>>> where(
    String field,
    Object? isEqualTo,
    Object? isNotEqualTo,
    Object? isLessThan,
    Object? isLessThanOrEqualTo,
    Object? isGreaterThan,
    Object? isGreaterThanOrEqualTo,
    Object? isNull,
  ) {
    // TODO: implement where
    throw UnimplementedError();
  }
}
