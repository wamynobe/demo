import 'package:freezed_annotation/freezed_annotation.dart';

import '../../repository/models/serializable.dart';

part '../models/photo_model.freezed.dart';
part '../models/photo_model.g.dart';

@freezed
class PhotoModel extends Serializable<PhotoModel> with _$PhotoModel {
  factory PhotoModel({
    @JsonKey(name: 'albumId') required int albumId,
    required int id,
    required String title,
    required String url,
    @JsonKey(name: 'thumbnailUrl') required String thumbnailUrl,
  }) = _PhotoModel;

  factory PhotoModel.fromJson(Map<String, dynamic> json) =>
      _$PhotoModelFromJson(json);
}
