import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app/data/service/service_locator.dart';
import 'app/lang/localization_service.dart';
import 'app/routes/app_pages.dart';

Future<void> main() async {
  //catch error synchronous
  FlutterError.onError = (details) => log(
        '''Caught error : ${details.exceptionAsString()}, StackTrace : ${details.stack}''',
      );
  await runZonedGuarded(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      await setupLocator();
      runApp(
        GetMaterialApp(
          title: 'Demo',
          initialRoute: AppPages.INITIAL,
          getPages: AppPages.routes,
          locale: Localization.locale,
          fallbackLocale: Localization.kFallBackLocale,
          translations: Localization(),
        ),
      );
    },
    //catch error as and can modify to show dialog to notify user
    // or send log to dev server
    (error, stack) => log('Caught error : $error , ${stack.toString()}'),
  );
}
